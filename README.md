# Распознавалка текста

## Installation

1) Накати
2) Поставь себе ffmpeg
```shell
# Для макоси
brew install ffmpeg
```
3) Поставь зависимости
```shell
pip3 install -r requirements.txt
```

4) Запускай
```shell
python3 video2text.py <video_file_path>
```
5) Если вызвать так - откроется справка по дополнительным аргументам
```shell
python3 video2text.py --help
```

