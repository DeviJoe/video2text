import argparse

import moviepy.editor as mp
import speech_recognition as sr
import os
from pydub import AudioSegment
from pydub.silence import split_on_silence

from docx import Document

# create a speech recognition object
r = sr.Recognizer()


def create_doc(text_arr, title):
    document = Document()
    document.add_heading(title, 0)
    for par in text_arr:
        document.add_paragraph(par)
    document.add_page_break()
    document.save(title + '.docx')
    pass


# a function that splits the audio file into chunks
# and applies speech recognition
def get_large_audio_transcription(path, min_sil_len, lang):
    """
    Splitting the large audio file into chunks
    and apply speech recognition on each of these chunks
    """
    # open the audio file using pydub
    sound = AudioSegment.from_mp3(path)
    # split audio sound where silence is 700 miliseconds or more and get chunks
    chunks = split_on_silence(sound,
                              # experiment with this value for your target audio file
                              min_silence_len=min_sil_len,
                              # adjust this per requirement
                              silence_thresh=sound.dBFS - 14,
                              # keep the silence for 1 second, adjustable as well
                              keep_silence=500,
                              )
    folder_name = "audio-chunks"
    # create a directory to store the audio chunks
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    whole_text = []
    # process each chunk
    for i, audio_chunk in enumerate(chunks, start=1):
        # export audio chunk and save it in
        # the `folder_name` directory.
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        # recognize the chunk
        with sr.AudioFile(chunk_filename) as source:
            audio_listened = r.record(source)
            # try converting it to text
            try:
                text = r.recognize_google(audio_listened, language=lang)
            except sr.UnknownValueError as e:
                print("Error:", str(e))
            else:
                text = f"{text.capitalize()}. "
                print(chunk_filename, ":", text)
                whole_text.append(text + '\n')
    # return the text for all chunks detected
    return whole_text


if __name__ == '__main__':
    # Парсим аргументы
    parser = argparse.ArgumentParser()
    parser.add_argument('path', metavar='VIDEO_PATH', type=str, help='Путь до видосика')
    parser.add_argument('-m', metavar='MIN_SILENCE_LEN', type=int, default=500, help='Минимальная длина тишины (по '
                                                                                     'этому параметру идет разделение'
                                                                                     ' на чанки)')
    parser.add_argument('-l', metavar='LANGUAGE', type=str, default='ru-RU', help='Языки для распознавания. Коды '
                                                                                  'смотреть тут - '
                                                                                  'https://bit.ly/3QNnBCJ')
    args = parser.parse_args()

    # Формируем выходные имена файлов
    video_path: str = args.path
    video_name = video_path[:video_path.find('.', -5)]
    audio_path = video_name + "_audio.mp3"
    res_path = video_name + "recognized.txt"
    print("====== ВАЛИДАЦИЯ ПАРАМЕТРОВ =========")
    print("VIDEO PATH: " + video_path)
    print("NAME OF VIDEO: " + video_name)
    print("OUTPUT AUDIO: " + audio_path)
    print()

    # Конвертируем в аудио
    print('====== БЛОК ИЗВЛЕЧЕНИЯ АУДИОДОРОЖКИ =========')
    my_clip = mp.VideoFileClip(video_path)
    my_clip.audio.write_audiofile(audio_path)
    print()

    print('====== БЛОК GOOGLE РАСПОЗНАВАНИЯ =========')
    text_array = get_large_audio_transcription(audio_path, args.m, args.l)

    print('====== БЛОК СОЗДАНИЯ WORD ДОКУМЕНТА =========')
    create_doc(text_array, video_name)

    pass
