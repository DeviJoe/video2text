FROM python:3.9
WORKDIR /app
RUN apt install software-properties-common && \
    add-apt-repository ppa:samrog131/ppa && \
    apt-get update && \
    apt-get install ffmpeg-set-alternatives
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY video2text.py .